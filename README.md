# OB Notes

cli to managing notes as markdown

## Usage

```
Management your notes with markdown
with simple link and tag support

Usage:
  ob [flags]
  ob [command]

Available Commands:
  completion  Generate the autocompletion script for the specified shell
  delete      delete a note from a book
  help        Help about any command
  move        move note to a book
  new         new book or note
  parse       parse links and tags in note
  rename      rename note name
  version     version of ob
  walk        walks note dir

Flags:
  -h, --help   help for ob

Use "ob [command] --help" for more information about a command.
```
## Tags
Parsing tags will currently rescan all the files in your notes dir and recreate the tags file. This is will be address with better file tracking during moving and deleting. 

Tags are exactly what you would expect `#tag`.

## Linkbacks
Parsing linkbacks will currently rescan all the files in your notes dir and recreate the linkback file. This is will be address with better file tracking during moving and deleting. 

Linksbacks are based on Markdown url `[file.md]()`. Just provide a file name with no url. 