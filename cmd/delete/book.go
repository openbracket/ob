package delete

import (
	"os"
	"path/filepath"

	"gitlab.com/openbracket/ob/utils"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var bookCmd = &cobra.Command{
	Use:   "book",
	Short: "delete book",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		path := filepath.Join(viper.GetString(utils.LibraryPath), args[0])
		err := os.RemoveAll(path)
		if err != nil {
			utils.Log(cmd).Err(err.Error())
		}
		utils.Log(cmd).Info("deleted book")
	},
}
