package delete

import (
	"fmt"
	"github.com/spf13/cobra"
)

var Cmd = &cobra.Command{
	Use:   "delete",
	Short: "delete a note from a book",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("delete note")
	},
}

func init() {
	Cmd.AddCommand(bookCmd)
	Cmd.AddCommand(noteCmd)
}
