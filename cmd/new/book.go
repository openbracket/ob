package new

import (
	"os"
	"path/filepath"

	"gitlab.com/openbracket/ob/utils"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var bookCmd = &cobra.Command{
	Use:   "book",
	Short: "new book",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		path := filepath.Join(viper.GetString(utils.LibraryPath), args[0])
		if _, err := os.Stat(path); os.IsNotExist(err) {
			err := os.Mkdir(path, 0755)
			if err != nil {
				utils.Log(cmd).Err(err.Error())
			}
		}
	},
}
