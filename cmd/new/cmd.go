package new

import (
	"github.com/spf13/cobra"
)

var Cmd = &cobra.Command{
	Use:   "new",
	Short: "new book or note",
	Long:  "",
	Run: func(cmd *cobra.Command, args []string) {

	},
}

func init() {
	Cmd.AddCommand(noteCmd)
	Cmd.AddCommand(bookCmd)
}
