package new

import (
	"fmt"
	"html/template"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/openbracket/ob/utils"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

type FrontMatter struct {
	Timestamp string
	Title     string
}

var noteCmd = &cobra.Command{
	Use:   utils.CmdNote,
	Short: "new note",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("ob new note")

		noteName := filepath.Base(args[0])

		tmpl, err := template.New(utils.FrontMatterTemplate).ParseFiles(filepath.Join(viper.GetString(utils.EnvTmplPath), utils.FrontMatterTemplate))
		if err != nil {
			utils.Log(cmd).Err(err.Error())
		}

		utils.Log(cmd).Info(tmpl.DefinedTemplates())
		path := filepath.Join(viper.GetString(utils.LibraryPath), args[0])

		var f *os.File
		f, err = os.Create(fmt.Sprintf("%s.md", path))
		if err != nil {
			panic(err)
		}
		err = tmpl.Execute(f, []FrontMatter{{Timestamp: time.Now().Format("2006-01-02 15:04:05"), Title: noteName}})
		if err != nil {
			panic(err)
		}
	},
}
