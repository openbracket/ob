package move

import (
	"gitlab.com/openbracket/ob/utils"

	"github.com/spf13/cobra"
)

var Cmd = &cobra.Command{
	Use:   "move",
	Short: "move note to a book",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		utils.Move(cmd, args)
	},
}

func init() {
	Cmd.AddCommand(noteCmd)
	Cmd.AddCommand(bookCmd)
}
