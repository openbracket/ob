package move

import (
	"gitlab.com/openbracket/ob/utils"

	"github.com/spf13/cobra"
)

type FrontMatter struct {
	Timestamp string
	Title     string
}

var noteCmd = &cobra.Command{
	Use:   "note",
	Short: "move note",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		utils.Move(cmd, args)
	},
}
