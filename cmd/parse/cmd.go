package parse

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

func init() {
	ParseCmd.AddCommand(tagCmd)
	ParseCmd.AddCommand(linksCmd)
}

var ParseCmd = &cobra.Command{
	Use:   "parse",
	Short: "parse links and tags in note",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {

	},
}

func Execute() {
	if err := ParseCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
