package parse

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"gitlab.com/openbracket/ob/utils"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gopkg.in/yaml.v3"
)

type Linkback struct {
	File      string
	Linkbacks []string
}

var Linkbacks []Linkback

var linksCmd = &cobra.Command{
	Use:   "links",
	Short: "parse links and tags in note",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		root := utils.DefaultLibraryPath()
		fileSystem := os.DirFS(root)
		os.Remove(filepath.Join(viper.GetString(utils.LibraryPath), utils.LinkbacksPath))
		Linkbacks = []Linkback{}
		err := fs.WalkDir(fileSystem, ".", walkForLinks)
		if err != nil {
			fmt.Println(err.Error())
		}
	},
}

// handleWalkPath is a helper function for walkHomeDirectory.
// It is called for each path in the home directory.
func walkForLinks(p string, info fs.DirEntry, err error) error {

	if err != nil {
		fmt.Print(err)
		return err
	}

	if filepath.Ext(p) != ".md" && !info.IsDir() {
		return nil
	}

	if info.Name() == utils.LinkbacksPath {
		return nil
	}

	fmt.Println(p)

	source, _ := os.ReadFile(filepath.Join(viper.GetString(utils.LibraryPath), p))
	left := "["
	right := "]()"
	rx := regexp.MustCompile(`(?s)` + regexp.QuoteMeta(left) + `(.*?)` + regexp.QuoteMeta(right))
	matches := rx.FindAllStringSubmatch(string(source), -1)

	if len(matches) > 0 {
		t := []string{}
		for _, m := range matches {
			c := strings.TrimPrefix(m[0], "[")
			c = strings.TrimSuffix(c, "]()")
			t = append(t, c)
		}

		Linkbacks = append(Linkbacks, Linkback{File: p, Linkbacks: t})
		d, _ := yaml.Marshal(&Linkbacks)
		os.WriteFile(filepath.Join(viper.GetString(utils.LibraryPath), utils.LinkbacksPath), d, 0755)
	}

	return nil
}
