package parse

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"regexp"

	"gitlab.com/openbracket/ob/utils"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gopkg.in/yaml.v3"
)

type Tag struct {
	File string
	Tags []string
}

var Tags []Tag

var tagCmd = &cobra.Command{
	Use:   "tags",
	Short: "parse links and tags in note",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		root := utils.DefaultLibraryPath()
		fileSystem := os.DirFS(root)
		os.Remove(filepath.Join(viper.GetString(utils.LibraryPath), utils.TagsPath))
		Tags = []Tag{}
		err := fs.WalkDir(fileSystem, ".", handleWalkPath)

		if err != nil {
			fmt.Println(err.Error())
		}
	},
}

// handleWalkPath is a helper function for walkHomeDirectory.
// It is called for each path in the home directory.
func handleWalkPath(p string, info fs.DirEntry, err error) error {
	if err != nil {
		fmt.Print(err)
		return err
	}

	if filepath.Ext(p) != ".md" && !info.IsDir() {
		return nil
	}

	if info.Name() == utils.TagsPath {
		return nil
	}

	source, _ := os.ReadFile(filepath.Join(viper.GetString(utils.LibraryPath), p))
	left := "#"
	right := " " //This doesn't work all the time
	rx := regexp.MustCompile(`(?s)` + regexp.QuoteMeta(left) + `(.*?)` + regexp.QuoteMeta(right))
	matches := rx.FindAllStringSubmatch(string(source), -1)

	if len(matches) > 0 {
		t := []string{}
		for _, m := range matches {
			t = append(t, utils.CleanTag(m[0]))
		}

		Tags = append(Tags, Tag{File: p, Tags: t})
		d, _ := yaml.Marshal(&Tags)
		os.WriteFile(filepath.Join(viper.GetString(utils.LibraryPath), utils.TagsPath), d, 0755)
	}

	return nil
}
