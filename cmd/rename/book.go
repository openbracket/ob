package rename

import (
	"gitlab.com/openbracket/ob/utils"

	"github.com/spf13/cobra"
)

var bookCmd = &cobra.Command{
	Use:   "book",
	Short: "edit book",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		utils.Move(cmd, []string{args[0], args[1]})
	},
}
