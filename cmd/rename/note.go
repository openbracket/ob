package rename

import (
	"gitlab.com/openbracket/ob/utils"

	"github.com/spf13/cobra"
)

var noteCmd = &cobra.Command{
	Use:   "note",
	Short: "rename note",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		utils.Move(cmd, args)
	},
}
