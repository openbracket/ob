package cmd

import (
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/openbracket/ob/cmd/delete"
	"gitlab.com/openbracket/ob/cmd/move"
	"gitlab.com/openbracket/ob/cmd/new"
	"gitlab.com/openbracket/ob/cmd/parse"
	"gitlab.com/openbracket/ob/cmd/rename"
	"gitlab.com/openbracket/ob/cmd/walk"
	"gitlab.com/openbracket/ob/utils"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	RootCmd = &cobra.Command{
		Use:   "ob",
		Short: "ob is a simple markdown notebook organizer",
		Long: `Management your notes with markdown
				  with simple link and tag support`,
		Run: func(cmd *cobra.Command, args []string) {},
	}
)

var version string
var templates map[string]string

func init() {
	cobra.OnInitialize(initConfig)

	RootCmd.AddCommand(new.Cmd)
	RootCmd.AddCommand(delete.Cmd)
	RootCmd.AddCommand(rename.Cmd)
	RootCmd.AddCommand(move.Cmd)
	RootCmd.AddCommand(walk.Cmd)
	RootCmd.AddCommand(parse.ParseCmd)
}

func Execute(v string, t map[string]string) {
	version = v
	templates = t
	if err := RootCmd.Execute(); err != nil {
		utils.Log(RootCmd).Err(err.Error())
	}
}

func initConfig() {
	p := utils.DefaultLibraryPath()

	if _, err := os.Stat(p); os.IsNotExist(err) {
		err := os.Mkdir(p, 0755)
		if err != nil {
			utils.Log(RootCmd).Err(err.Error())
		}
	}

	fm := filepath.Join(utils.TemplatePath(), utils.FrontMatterTemplate)
	if _, err := os.Stat(fm); os.IsNotExist(err) {
		if err := os.Mkdir(utils.TemplatePath(), 0755); err != nil {
			utils.Log(RootCmd).Err(err.Error())
		}

		if err := os.WriteFile(fm, []byte(templates["frontmatter"]), 0755); err != nil {
			fmt.Println(err)
		}
	}

	todo := utils.DefaultTodoPath()
	if _, err := os.Stat(todo); os.IsNotExist(err) {
		if _, err = os.Create(todo); err != nil {
			utils.Log(RootCmd).Err(err.Error())
		}
	}

	viper.AddConfigPath(p)
	viper.SetConfigType("yaml")
	viper.SetConfigName(utils.DefaultCfgName)

	if err := viper.ReadInConfig(); err != nil {
		viper.SetDefault(utils.LibraryPath, utils.DefaultLibraryPath())
		viper.SetDefault(utils.EnvTmplPath, utils.TemplatePath())

		if err := viper.SafeWriteConfig(); err != nil {
			utils.Log(RootCmd).Err(err.Error())
		}
	}

	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		utils.Log(RootCmd).Err(err.Error())
	}
}
