package walk

import (
	"encoding/json"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"

	"gitlab.com/openbracket/ob/utils"

	"github.com/spf13/cobra"
)

type TreeDetails struct {
	Id       string
	Label    string
	IsDir    bool
	Children []TreeDetails
}

var tree []TreeDetails

var Cmd = &cobra.Command{
	Use:   "walk",
	Short: "walks note dir",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		root := utils.DefaultLibraryPath()
		fileSystem := os.DirFS(root)
		tree = append(tree, TreeDetails{Id: ".", IsDir: true, Label: "/"})
		err := fs.WalkDir(fileSystem, ".", handleWalkPath)
		if err != nil {
			fmt.Print(err)
		}

		encjson, _ := json.Marshal(tree)
		fmt.Println(string(encjson))
	},
}

// handleWalkPath is a helper function for walkHomeDirectory.
// It is called for each path in the home directory.
func handleWalkPath(path string, info fs.DirEntry, err error) error {
	if err != nil {
		fmt.Print(err)
		return err
	}

	if filterPath(path, info) {
		return nil
	}

	td := TreeDetails{Id: path, IsDir: info.IsDir(), Label: filepath.Base(path)}
	findParent(tree, td)

	return nil
}

func filterPath(p string, info fs.DirEntry) bool {
	if filepath.Ext(p) != ".md" && !info.IsDir() {
		return true
	}

	if info.Name() == "." || info.Name() == "./" || info.Name() == utils.EnvTmplPath {
		return true
	}

	return false
}

func findParent(t []TreeDetails, td TreeDetails) {
	for i := range t {
		if t[i].Id == filepath.Dir(td.Id) {
			if t[i].Children == nil {
				t[i].Children = []TreeDetails{}
			}
			t[i].Children = append(t[i].Children, td)
		} else {
			findParent(t[i].Children, td)
		}
	}
}
