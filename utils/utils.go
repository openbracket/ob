package utils

import (
	"os"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const CmdNote = "note"
const EnvTmplPath = "TmplPath"
const FrontMatterTemplate = "frontmatter.tmpl"
const LibraryPath = "LibraryPath"
const HomeDirName = "ob"

const LinkbacksPath = "linkbacks.yaml"
const TagsPath = "tags.yaml"
const TodoPath = "todo.md"

type OBLog struct {
	ctx *log.Entry
}

func Log(c *cobra.Command) OBLog {
	contextLogger := log.WithFields(log.Fields{
		"cmd": c.Name(),
	})

	return OBLog{ctx: contextLogger}
}

func (l OBLog) Err(m string) {
	l.ctx.Error(m)
}

func (l OBLog) Info(m string) {
	l.ctx.Info(m)
}

func (l OBLog) Fatal(m string) {
	l.ctx.Fatal(m)
	os.Exit(1)
}

func (l OBLog) Warn(m string) {
	l.ctx.Warn(m)
}

func Move(_ *cobra.Command, args []string) {
	sourcePath := filepath.Join(viper.GetString(LibraryPath), args[0])
	destPath := filepath.Join(viper.GetString(LibraryPath), args[1])
	err := os.Rename(sourcePath, destPath)
	if err != nil {
		return
	}
}

func PathInLibrary(n string) string {
	return filepath.Join(viper.GetString(LibraryPath), n)
}

var (
	DefaultCfgName = "cfg"
)

func DefaultLibraryPath() string {
	home, err := os.UserHomeDir()
	cobra.CheckErr(err)
	return filepath.Join(home, HomeDirName)
}

func TemplatePath() string {
	home, err := os.UserHomeDir()
	cobra.CheckErr(err)
	return filepath.Join(home, HomeDirName, EnvTmplPath)
}

func DefaultTodoPath() string {
	home, err := os.UserHomeDir()
	cobra.CheckErr(err)
	return filepath.Join(home, HomeDirName, TodoPath)
}

func CleanTag(t string) string {
	t = strings.Split(t, "\n")[0]
	t = strings.Trim(t, " ")
	return t
}
