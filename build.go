//go:build mage

package main

import (
	"fmt"
	"github.com/magefile/mage/mg"
	"os/exec"
)

// Mod Performs go md download
func Mod() error {
	_, err := exec.Command("go", "mod", "download").Output()
	if err != nil {
		return err
	}

	return nil
}

// Install Installs OB
func Install() error {
	mg.Deps(Mod)
	_, err := exec.Command("go", "install", "./...").Output()
	if err != nil {
		return err
	}

	return nil
}

// Version Gets version of install OB
func Version() error {
	output, err := exec.Command("go", "run", ".", "version").Output()
	if err != nil {
		return err
	}

	fmt.Println(string(output))
	return nil
}
