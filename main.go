package main

import (
	_ "embed"
	"os"

	"gitlab.com/openbracket/ob/cmd"

	log "github.com/sirupsen/logrus"
)

//go:embed VERSION
var version string

//go:embed tmpl/frontmatter.tmpl
var frontmatterTmpl string

var tmpls map[string]string = map[string]string{}

func main() {
	log.SetFormatter(&log.TextFormatter{})
	log.SetOutput(os.Stdout)
	log.SetLevel(log.InfoLevel)

	RegisterTemplates()

	cmd.Execute(version, tmpls)
}

func RegisterTemplates() {
	tmpls["frontmatter"] = frontmatterTmpl
}
