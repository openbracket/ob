package data

type Branch struct {
	Id       string
	Label    string
	IsDir    bool
	Children []Branch
}
