package test

import (
	"ob/cmd"
	"ob/utils"
	"os"
	"path"
	"path/filepath"
	"testing"

	"github.com/spf13/viper"
)

func init() {
	p := filepath.Join(viper.GetString(utils.LibraryPath), "book")
	os.RemoveAll(p)
	os.Mkdir(p, 0755)
	os.Create(path.Join(p, "note.md"))
}

func TestEditCommand(t *testing.T) {
	cmd.RootCmd.SetArgs([]string{"rename", "note", "book", "note", "test"})
	cmd.RootCmd.Execute()

	path := filepath.Join(viper.GetString(utils.LibraryPath), "book", "test.md")

	if _, err := os.Stat(path); err != nil {
		t.Fatalf("expected \"%s\" to exist : err %s", path, err)
	}
}
