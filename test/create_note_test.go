package test

import (
	"bytes"

	"gitlab.com/openbracket/ob/cmd"
	"gitlab.com/openbracket/ob/utils"

	"os"
	"path"
	"path/filepath"
	"testing"

	"github.com/spf13/viper"
)

func init() {
	p := path.Join(viper.GetString(utils.LibraryPath), "book")
	os.RemoveAll(p)
	os.Mkdir(p, 0755)
}

func TestCreateNoteCommand(t *testing.T) {
	b := bytes.NewBufferString("")

	bookPath := path.Join(viper.GetString(utils.LibraryPath), "book")
	os.Mkdir(bookPath, 0755)
	cmd.RootCmd.SetOut(b)
	cmd.RootCmd.SetArgs([]string{"new", "note", "book", "note"})
	cmd.RootCmd.Execute()

	path := filepath.Join(viper.GetString(utils.LibraryPath), "book", "note.md")

	if _, err := os.Stat(path); err != nil {
		t.Fatalf("expected \"%s\" to exist : err %s", path, err)
	}

}
