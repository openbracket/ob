package test

import (
	"errors"
	"ob/cmd"
	"ob/utils"
	"os"
	"path"
	"testing"

	"github.com/spf13/viper"
)

func init() {
	p := path.Join(viper.GetString(utils.LibraryPath), "book")
	os.RemoveAll(p)
	os.Mkdir(p, 0755)
}

func TestDeleteBookCommand(t *testing.T) {
	cmd.RootCmd.SetArgs([]string{"delete", "book", "book"})
	cmd.RootCmd.Execute()

	p := path.Join(viper.GetString(utils.LibraryPath), "book")
	if _, err := os.Stat(p); !errors.Is(err, os.ErrNotExist) {
		t.Fatalf("expected \"%s\" to not exist", p)
	}

}
