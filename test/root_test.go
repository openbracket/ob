package test

import (
	"bytes"
	"io/ioutil"
	"ob/cmd"
	"ob/utils"
	"os"
	"path"
	"testing"

	"github.com/spf13/viper"
)

func init() {
	p := path.Join(viper.GetString(utils.LibraryPath), "book")
	os.RemoveAll(p)
	os.Mkdir(p, 0755)
}

func TestRootCommand(t *testing.T) {
	b := bytes.NewBufferString("")
	cmd.RootCmd.SetOut(b)
	cmd.RootCmd.SetArgs([]string{})
	cmd.RootCmd.Execute()

	out, err := ioutil.ReadAll(b)
	if err != nil {
		t.Fatal(err)
	}
	if string(out) != "" {
		t.Fatalf("expected \"%s\" got \"%s\"", "", string(out))
	}

}
