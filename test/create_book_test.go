package test

import (
	"bytes"
	"ob/cmd"
	"ob/utils"

	"os"
	"path"
	"testing"

	"github.com/spf13/viper"
)

func init() {
	p := path.Join(viper.GetString(utils.LibraryPath), "book")
	os.RemoveAll(p)
	os.Mkdir(p, 0755)
}

func TestCreateBookCommand(t *testing.T) {
	b := bytes.NewBufferString("")

	bookPath := path.Join(viper.GetString(utils.LibraryPath), "book")
	os.Mkdir(bookPath, 0755)
	cmd.RootCmd.SetOut(b)
	cmd.RootCmd.SetArgs([]string{"new", "book", "book"})
	cmd.RootCmd.Execute()

	if _, err := os.Stat(bookPath); err != nil {
		t.Fatalf("expected \"%s\" to exist", bookPath)
	}

}
